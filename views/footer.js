document.write('\
<footer>\
    <p class="socials">\
        <a target="_blank" rel="noopener noreferrer" href="https://github.com/tweimann">@tweimann</a> on github<br>\
        <a target="_blank" rel="noopener noreferrer" href="https://www.youtube.com/watch?v=dQw4w9WgXcQ">@tweimann</a> on instagram<br>\
        <a target="_blank" rel="noopener noreferrer" href="mailto:contact@tweimann.xyz">contact@tweimann.xyz</a><br>\
    </p>\
    <p class="watermark">\
            <a target="_blank" rel="noopener noreferrer" href="https://github.com/tweimann">tweimann</a> 2021 \u2022 Made with \
            <a target="_blank" rel="noopener noreferrer" class="heart" href="https://github.com/tweimann/tweimann.xyz">\u2764</a> in Germany \u2022 \
            <a href="imprint.html">Imprint</a>\
    </p>\
</footer>');